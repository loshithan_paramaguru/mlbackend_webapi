import asyncio
import joblib
from flask import Flask, request, jsonify
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String
import os
from dotenv import load_dotenv
from transformers import pipeline

# Load environment variables from .env file
load_dotenv()

app = Flask(__name__)

# Create the engine with the corrected connection string
SQLALCHEMY_DATABASE_URI = f"mysql+asyncmy://{os.getenv('USER')}:{os.getenv('PASSWORD')}@{os.getenv('DATABASE_URL')}/empfee"
engine = create_async_engine(SQLALCHEMY_DATABASE_URI, echo=True)

# Create an async sessionmaker
async_session = sessionmaker(
    bind=engine,
    expire_on_commit=False,
    class_=AsyncSession
)

Base = declarative_base()

# Define your database models
class Answer(Base):
    __tablename__ = 'answer'
    AnswerId = Column(Integer, primary_key=True)

class Feedback(Base):
    __tablename__ = 'feedback'
    Id = Column(Integer, primary_key=True)
    AnswerId = Column(Integer, nullable=False)
    Output = Column(String(255), nullable=False)
    Score = Column(String(255), nullable=False)

    def __repr__(self):
        return f"<Feedback(Id={self.Id}, AnswerId={self.AnswerId}, Output='{self.Output}', Score='{self.Score}')>"

# Initialize the sentiment analysis pipeline and models
sentiment_pipeline = pipeline(
    "sentiment-analysis",
    model="distilbert-base-uncased-finetuned-sst-2-english",
    revision="af0f99b"
)

@app.route('/built-text-analysis', methods=['POST'])
async def prebuilt_analyze_text():
    if not request.is_json:
        return jsonify({'error': 'Request must be JSON'}), 400

    data = request.get_json()
    if not isinstance(data, list):
        return jsonify({'error': 'Request data must be a JSON array of objects.'}), 400

    feedbacks = []
    async with asyncio.Lock():  # Ensure async tasks are handled correctly
        async with async_session() as session:
            async with session.begin():
                for item in data:
                    output = item.get('value')
                    if output:
                        sentiment_result = await asyncio.to_thread(sentiment_pipeline, output)
                        predicted_label = sentiment_result[0]['label']
                        sentiment_score = sentiment_result[0]['score']
                        result = {"label": predicted_label, "score": sentiment_score}

                        feedback = Feedback(
                            AnswerId=item.get('answerId'),
                            Output=result['label'],
                            Score=str(result['score'])
                        )
                        session.add(feedback)
                        feedbacks.append({'AnswerId': item.get('answerId'), 'Output': result['label'], 'Score': result['score']})
                await session.commit()

    return jsonify(feedbacks), 200

@app.route('/')
def index():
    return jsonify({'name': 'alice', 'email': 'alice@outlook.com'})

if __name__ == '__main__':
    # Production configuration (replace with your server settings):
    app.run(host='0.0.0.0', port=80)

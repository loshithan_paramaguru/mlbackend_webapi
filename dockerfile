# Use an official Python runtime as a parent image
FROM python:3.9-slim

# Set environment variables
ENV PYTHONUNBUFFERED=1
ENV PIP_DEFAULT_TIMEOUT=1000

# Install system dependencies and set up build environment
RUN apt-get update \
    && apt-get install -y \
        default-libmysqlclient-dev \
        build-essential \
        python3-dev \
        libffi-dev \
        libssl-dev \
        pkg-config \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Upgrade pip to the latest version
RUN pip install --upgrade pip

# Check pip version
RUN pip --version

# Set the working directory in the container
WORKDIR /app

# Copy the requirements file into the container
COPY requirement.txt /app/

# Install any needed packages specified in requirements.txt using a faster mirror
RUN pip install --no-cache-dir --prefer-binary -r requirement.txt --index-url https://pypi.org/simple --retries 2 --timeout 1000

# Copy the current directory contents into the container at /app
COPY . /app

# Expose port 80 to the outside world
EXPOSE 80

# Define environment variables
ENV FLASK_APP=app.py

# Run the Flask app
CMD ["flask", "run", "--host=0.0.0.0", "--port=80"]
